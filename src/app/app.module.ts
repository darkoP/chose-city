import { BrowserModule } from '../../node_modules/@angular/platform-browser';
import { NgModule } from '../../node_modules/@angular/core';
import { FormsModule, ReactiveFormsModule } from '../../node_modules/@angular/forms';
import { HttpModule } from '../../node_modules/@angular/http';

import { AppComponent } from './app.component';
import {CityItemComponent} from './cities/city-item.component';
import {CityListComponent} from './cities/city-list.component';
import {CityService} from './cities/city.service';
import {CitySearchComponent} from './cities/city-search.component';
import {ProgressBarComponent} from './cities/progress-bar.component';
import {AutocompleteComponent} from './cities/autocomplete.component';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ResultComponent} from './result/result.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'result',
    component: ResultComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    CityItemComponent,
    CityListComponent,
    CitySearchComponent,
    ProgressBarComponent,
    AutocompleteComponent,
    HomeComponent,
    ResultComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [CityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
