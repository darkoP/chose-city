import {Component, OnInit} from '@angular/core';
import {CityService} from './city.service';

@Component({
  selector: 'city-list',
  templateUrl: './city-list.component.html'
})
export class CityListComponent implements OnInit{
  cityItems: String[];

  constructor(private _cityService: CityService) {}

  removeItem(item: String) {
    this._cityService.removeCityItem(item);
  }

  ngOnInit(): any {
    this.cityItems = this._cityService.getCityItems();
  }

}
