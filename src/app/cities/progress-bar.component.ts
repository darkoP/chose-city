import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'progress-bar',
  templateUrl: './progress-bar.component.html'
})
export class ProgressBarComponent implements OnInit{

  @Input()
  percentage: number;

  onNotify(percentage: number): any {
    alert(percentage);
  }

  ngOnInit(): any {
    console.log(this.percentage)
  }
}
