import {Component, Input, OnInit, Output} from '@angular/core';
import {NgForm, FormBuilder, Validators, FormGroup} from '@angular/forms';
import {CityService} from './city.service';
import {timer} from 'rxjs/observable/timer';
import {Router} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import {EventEmitter} from '@angular/common/src/facade/async';

@Component({
  selector: 'city-search',
  templateUrl: './city-search.component.html',
})
export class CitySearchComponent implements OnInit{

  public chosen= [];
  public selected = null;
  public autocompleteResults = [];
  public showAutocompleteResults: Boolean = false;
  public city = null;
  public cityForm: FormGroup;
  public cityAlreadyInTheList = false;
  public ticks = 0;
  public subscription;
  public cityDoesNotExist = false;
  cities = [];
  correct= [];
  area;

  constructor(private _cityService: CityService, fb: FormBuilder, private router: Router) {
    this.cityForm = fb.group({
      'city': [null, Validators.required]
    })
  }

  onSubmit(f: NgForm) {
    const city = f.value.city;
    this._cityService.searchCityData(city)
      .subscribe(
        cities => {
          this.cityDoesNotExist = false;

          if(this.chosen.indexOf(city) > -1) {
            this.cityAlreadyInTheList = true;
          } else {
            this.cityAlreadyInTheList = false;
          }

          if(cities.length && !(this.chosen.indexOf(city) > -1)) {
            this._cityService.addCityItem(city);
            this.chosen.push(city);
            this.cityForm.reset();
            this.city = null;
          } else {
          this.cityDoesNotExist = true
          }
        }
      );
  }

  findCities(city) {
    this._cityService.searchCityData(city)
      .subscribe(
        cities => {
          this.autocompleteResults = cities;
          this.showAutocompleteResults = !!cities.length;
        }
      );
  }

  onNotifyAutocomplete(chosenCity): void {
    this.city = chosenCity;
    this.showAutocompleteResults = false;
    this.cityAlreadyInTheList = false;
  }

  ngOnInit(): any {
    this._cityService.resetCityItems();
    this.chosen = [];
    this.city = null;

    this._cityService.getCity()
      .subscribe(
        resCityData => this.cities = resCityData.ponudjene
      );
    this._cityService.getCorrect()
      .subscribe(
        resCorrectData => {
          this.correct = resCorrectData.tacno
          console.log(this.correct)
        }
      );
    this._cityService.getTime()
      .subscribe(
        resTimeData => {
          this.ticks = resTimeData.vreme;
          let counter = timer(1000, 1000);
          this.subscription = counter.subscribe(t => {
            if (this.ticks === 0) {
              this.subscription.unsubscribe();
              this.isCorrect();
            } else {
              this.ticks -= 1;
            }
          })
        }
      );
    this._cityService.getArea()
      .subscribe(
        resAreaData => this.area = resAreaData.oblast
      );
  }

  @Output()
  notify: EventEmitter<number> = new EventEmitter<number>();

  correctAnswer = 0;

  @Input()
  percentage = 0;

  isCorrect() {
    this.subscription.unsubscribe();
    this.correctAnswer = 0
    this.chosen.filter((chosen)=>{
      if(this.correct.indexOf(chosen) > -1) {
        this.correctAnswer++;
      }
    });

    this.percentage = (100*this.correctAnswer)/(this.correct.length);
    console.log(this.percentage);
    this._cityService.setResult(this.percentage, this.correctAnswer, this.correct);

    this.router.navigate(['/result']);
  }

}
