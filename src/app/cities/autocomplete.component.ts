import {Component, Input, Output} from '@angular/core';
import {EventEmitter} from '@angular/common/src/facade/async';

@Component({
  selector: 'autocomplete',
  templateUrl: './autocomplete.component.html'
})
export class AutocompleteComponent {
  @Output()
  onNotifyAutocomplete: EventEmitter<number> = new EventEmitter<number>();

  @Input()
  autocompleteResults: number;

  setValue(city) {
    this.onNotifyAutocomplete.emit(city);
  }
}
