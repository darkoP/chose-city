import {Component, Input} from '@angular/core';
import {CityItem} from './city-item';

@Component({
  selector: 'city-item',
  templateUrl: './city-item.component.html'
})

export class CityItemComponent {
  @Input('item') cityItem: CityItem;

}
