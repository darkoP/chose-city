import {Injectable} from '@angular/core';
import {CITY_ITEMS} from './city.data';
import {Http,Response} from '../../../node_modules/@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CityService {

  result = {
    percentage: '',
    correctAnswers: '',
    allCorrectCities: ''
  };
  private _url: string = './assets/podaci.json';

  getCityItems() {
    return CITY_ITEMS;
  }

  constructor(private _http: Http) {}

  setResult(percentage, correctAnswers, allCorrectCities) {
    this.result.percentage = percentage;
    this.result.correctAnswers = correctAnswers;
    this.result.allCorrectCities = allCorrectCities;
  }

  getResult() {
    return this.result;
  }
  addCityItem(cityItem: String) {
    CITY_ITEMS.push(cityItem);
  }

  removeCityItem(cityItem: String) {
    CITY_ITEMS.splice(CITY_ITEMS.indexOf(cityItem), 1);
  }
  resetCityItems() {
    CITY_ITEMS.length = 0;
  }

  searchCityData(cityName: string) {
    return this._http.get(this._url)
      .map((response : Response) => {
        const cities = response.json().ponudjene;
        const possibleCities = [];

        for (let city of cities) {
          if (city.toLocaleLowerCase().indexOf(cityName.toLocaleLowerCase()) > -1) {
            possibleCities.push(city);
          }
        }
        return possibleCities;
      })
  }

  getCity(){
    return this._http.get(this._url)
      .map((response : Response) => response.json());
  }

  getCorrect() {
    return this._http.get(this._url)
      .map((response : Response) => response.json());
  }

  getTime() {
    return this._http.get(this._url)
      .map((response : Response) => response.json());
  }

  getArea() {
    return this._http.get(this._url)
      .map((response : Response) => response.json());
  }

}
