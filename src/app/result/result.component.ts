import {Component, OnInit} from '@angular/core';
import {CityService} from '../cities/city.service';
import {Router} from '@angular/router';

import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'result-component',
  templateUrl: './result.component.html'
})
export class ResultComponent implements OnInit{
  public result;
  public percentage = 0;
  public correctAnswers;
  public allCorrectCities;
  public showResults = false;
  constructor(private _cityService: CityService, private router: Router) {}
  ngOnInit(): any {
    this.result = this._cityService.getResult();
    console.log("nisam jebeno definisan")
    console.log(Object.keys(this.result))


    if(!Object.keys(this.result).length) {
      this.router.navigate(['/']);
    } else {
      console.log("Nema resultataaaa")
      this.percentage = this.result.percentage;
      this.correctAnswers = this.result.correctAnswers;
      this.allCorrectCities = this.result.allCorrectCities.length;

      setTimeout(() => {
        this.showResults = true;
      }, 200)
    }
  }

}
