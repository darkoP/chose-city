import {Component} from '@angular/core';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html'
})
export class HomeComponent{
  percentage = 0;
  public showResults = false;

  onNotify(message): void {
    this.percentage = message;
    this.showResults = true;
  }
}
