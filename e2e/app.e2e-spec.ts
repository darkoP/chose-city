import { HitTheCityPage } from './app.po';

describe('hit-the-city App', function() {
  let page: HitTheCityPage;

  beforeEach(() => {
    page = new HitTheCityPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
